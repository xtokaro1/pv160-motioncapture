﻿using System.Collections.Generic;
using UnityEngine;

public class FreezeOutline : MonoBehaviour
{
    public float timeUntilSpawn;
    [SerializeField] private float timeBetweenSpawn = 1.5f;
    [SerializeField] private int trailCount = 16;
    [SerializeField] private GameObject rootBone;
    [SerializeField] private GameObject mesh;
    [SerializeField] private Transform avatarTransform;

    private readonly Queue<GameObject> trails = new Queue<GameObject>();
    private readonly Queue<GameObject> rootBones = new Queue<GameObject>();

    private void CopyBones(GameObject targetObject)
    {
        targetObject.transform.position = avatarTransform.position;
        targetObject.transform.rotation = avatarTransform.rotation;
        var target = targetObject.GetComponent<SkinnedMeshRenderer>();
        var newRootBone = Instantiate(rootBone);
        newRootBone.name = rootBone.name;
        rootBones.Enqueue(newRootBone);
        target.rootBone = newRootBone.transform;
        Transform[] newBones = new Transform[target.bones.Length];
        var bones = newRootBone.transform.GetComponentsInChildren<Transform>();
        for (int i = 0; i < target.bones.Length; i++)
        {
            foreach (var newBone in bones)
            {
                if (newBone.name == target.bones[i].name)
                {
                    if (i == 8)
                    {
                        newBone.position += avatarTransform.position;    
                    }
                    newBones[i] = newBone;
                    break;
                }
            }
        }
        target.bones = newBones;
    }
    void Update()
    {
        if (timeUntilSpawn <= 0)
        {
            var newMesh = Instantiate(mesh);
            CopyBones(newMesh);
            trails.Enqueue(newMesh);
            while (trails.Count > trailCount) // odstranenie extra trailov
            {
                Destroy(trails.Dequeue());
                Destroy(rootBones.Dequeue());
            }
            timeUntilSpawn += timeBetweenSpawn; // pridanie casu
        }
        else
        {
            timeUntilSpawn -= Time.deltaTime; // odobratie casu
        }
    }
}